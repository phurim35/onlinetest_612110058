﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class EndGameManager : MonoBehaviourPun
{
    bool gameHasEnd = false;
    public PlayerMovement movement;

    public GameObject gameOverUI;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void EndGame()
    {
        if (photonView.IsMine)
        {
            if (gameHasEnd == false)
            {
                gameOverUI.SetActive(true);

                
                movement.enabled=false;
                
                gameHasEnd = true;

                Debug.Log("Game Over");
            }
        }
        else
            gameOverUI.SetActive(false);




    }
}
