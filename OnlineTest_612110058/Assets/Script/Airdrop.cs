﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Airdrop
{
    public static Vector3 RandomPosition(float yOffset)
    {
        
        var spawnPosition = new Vector3(
            Random.Range(-7.58f, 76.51f),
            yOffset,
            Random.Range(-4, 43));
        return spawnPosition;
    }
    public static Quaternion RandomRotion()
    {
        var spawnRotion = Quaternion.Euler(
            1.0f,
            Random.Range(0,0),
            0.0f);
        return spawnRotion;
    }
}
