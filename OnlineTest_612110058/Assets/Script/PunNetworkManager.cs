﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Pun.UtilityScripts;

public class PunNetworkManager : ConnectAndJoinRandom
{
    #region Singleton
    public static PunNetworkManager singleton;
    private void Awake()
    {
        if (singleton != null)
        {
            return;
        }
        singleton = this;
    }
    #endregion

    public GameObject HealingPrefab;
    public int numberOfHealing=5;
    
    

    public List<GameObject> spawnPoint=new List<GameObject>(10);
    public GameObject[] currentPoint;
    int index;

    public List<GameObject> playerPos;
    public GameObject[] currentPos;
    GameObject currentPosPos;
    int ran;

    


    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;

    
    

    private void Update()
    {
        if (PhotonNetwork.IsMasterClient != true)
            return;
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        

        if (PunUserNetControl.LocalPlayerInstance == null)
        {
            Debug.Log("We are Instantiating LocalPlayer from" + SceneManagerHelper.ActiveSceneName);
            
            
                PunNetworkManager.singleton.SpawnPlayer();


        }
        else
        {
            Debug.Log("Ignoring scene load for" + SceneManagerHelper.ActiveSceneName);
        }
    }
    public void SpawnPlayer()
    {
        Camera.main.gameObject.SetActive(false);



        RandomPlayerPosition();

        
        
            PhotonNetwork.Instantiate(GamePlayerPrefab.name,
            currentPosPos.transform.position, Quaternion.identity, 0);
            Debug.Log("" + spawnPoint.Count);
        
            
        SetPotion();
        
    }
    void SetPotion()
    {
        for (int i = 0; i < 10; i++)
        {
            index = Random.Range(0, spawnPoint.Count);
            
            currentPoint[i] = spawnPoint[index];
            spawnPoint.RemoveAt(index);
            PhotonNetwork.Instantiate(HealingPrefab.name
                    , currentPoint[i].transform.position
                    , Quaternion.Euler(0, 0, 0)
                    , 0);
            
        }
    }
    void RandomPlayerPosition()
    {
        for (int i = 0; i < 4; i++)
        {
            ran = Random.Range(0, playerPos.Count);
            currentPos[i] = playerPos[ran];
            currentPosPos = currentPos[i];
            
            playerPos.RemoveAt(ran);
        }
    }
    
    


}
