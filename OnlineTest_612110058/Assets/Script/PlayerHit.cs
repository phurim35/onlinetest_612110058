﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;


public class PlayerHit : MonoBehaviourPunCallbacks, IPunObservable
{
    public PlayerMovement thisPlayer;
    public Text damage;

    public int m_AmountDamage = 5;

    public PlayerMovement playerMovement;

    


    public void OnGUI()
    {
        if (photonView.IsMine)
            damage.text = "Damage : " + m_AmountDamage;
    }
    private void Start()
    {
        thisPlayer.GetComponent<PlayerMovement>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!photonView.IsMine)
            return;
        
        if (thisPlayer.isAttacking==true)
        {
            if (collision.CompareTag("Player"))
            {

                Debug.Log("Hit Player");

                PunUserNetControl tempOther = collision.gameObject.GetComponent<PunUserNetControl>();
                if (tempOther != null)
                    Debug.Log("Attack to other ViewID : " + tempOther.photonView.ViewID);


                PunHealth tempHealth = collision.gameObject.GetComponent<PunHealth>();
                if (tempHealth != null)
                    tempHealth.TakeDamage(m_AmountDamage, photonView.ViewID);

                



                



            }
        }
         
    }
    [PunRPC]
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(m_AmountDamage);
        }
        else
        {
            m_AmountDamage = (int)stream.ReceiveNext();
        }
    }
    public void PlusDamage(int amount)
    {
        m_AmountDamage += amount;
    }



    //private void OnDestroy()
    //{
    //    if (!photonView.IsMine)
    //        return;
    //    PhotonNetwork.Destroy(this.gameObject);
    //}

}
