﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public enum PlayerState
    {
        walk,
        attack
    }

[RequireComponent(typeof(PhotonAnimatorView))]
public class PlayerMovement : MonoBehaviourPun
{
    public float movespeed=5f;
    public Rigidbody2D rb;
    public Rigidbody2D rb2;
    public Vector3 movement;
    public Animator animator;

    PlayerState currentState;

    Knockback isKnock;

    public bool isAttacking=false;
    




    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb2 = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        movement = Vector3.zero;
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        if (Input.GetMouseButtonDown(0)&&currentState!=PlayerState.attack)
        {
            
            isAttacking = true;
            StartCoroutine(AttackCo());
        }
        else if (currentState == PlayerState.walk)
        {
            UpdateAnimation();
        }
        

        
        
    }
    void FixedUpdate()
    {
        rb.MovePosition(transform.position + movement * movespeed * Time.deltaTime);
        isKnock = rb.gameObject.GetComponent<Knockback>();

        

        
        if (isKnock.knock == true)
            OnKnockback();
        
    }
    private IEnumerator AttackCo()
    {
        animator.SetBool("Attacking", true);
        currentState = PlayerState.attack;
        yield return null;
        animator.SetBool("Attacking", false);
        yield return new WaitForSeconds(0.2f);
        currentState = PlayerState.walk;
        isAttacking = false;


    }
    
    private void UpdateAnimation()
    {
        if (movement != Vector3.zero)
        {
            animator.SetFloat("Horizontal", movement.x);
            animator.SetFloat("Vertical", movement.y);

            animator.SetBool("Moving", true);
        }
        else
            animator.SetBool("Moving", false);
    }
    

    
    public void OnKnockback()
    {
        rb.MovePosition(transform.position - movement * isKnock.thrust * Time.deltaTime);
        
    }

    








}
