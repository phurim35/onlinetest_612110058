﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class RedPotion : MonoBehaviourPun
{
    
    public int healingPoint = 1;
    public int damagePoint=5;

    
    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Test other : " + other.gameObject.name);
        if (other.gameObject.CompareTag("Player"))
        {
            PunHealth otherHeal = other.gameObject
                .GetComponent<PunHealth>();
            otherHeal.Healing(healingPoint);

            PlayerHit otherHit = other.gameObject
                .GetComponent<PlayerHit>();
            otherHit.PlusDamage(damagePoint);

            //send RPC 
            photonView.RPC("PunRPCHealing", RpcTarget.All);
            
            
        }   
    }
    public void Smash()
    {
        Destroy(this.gameObject);
    }
    [PunRPC]
    private void PunRPCHealing()
    {
        Destroy(this.gameObject);
        for (int i = 0; i < PunNetworkManager.singleton.spawnPoint.Count; i++) ;
        
        
    }
    private void OnDestroy()
    {
        if (!photonView.IsMine)
            return;
        this.gameObject.SetActive(false);
    }

}
