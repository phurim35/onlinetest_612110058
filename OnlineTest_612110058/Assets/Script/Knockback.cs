﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonRigidbody2DView))]
public class Knockback : MonoBehaviourPun
{
   

    public float thrust;
    public float knockTime;

    Rigidbody2D rb;
    
    public bool knock=false;
    

    

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!photonView.IsMine)
        {
            return;
        }
        if (other.gameObject.CompareTag("Player"))
        {

            knock = true;
            //rb = other.GetComponent<Rigidbody2D>();
            //Vector2 difference = rb.transform.position - transform.position;
            //difference = difference.normalized * thrust;

            //rb.AddForce(difference, ForceMode2D.Impulse);

            StartCoroutine(KnockCo());

        }
        



    }


    public IEnumerator KnockCo()
    {
        
            //Debug.Log("Knock : ");
            yield return new WaitForSeconds(knockTime);
            
            knock = false;
        
    }
    


}
