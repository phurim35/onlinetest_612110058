﻿using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class PunHealth : MonoBehaviourPunCallbacks, IPunObservable
{
    public const int maxHealth = 200;
    public int currentHealth = maxHealth;
    public Text health;

    public HealthBar healthBar;

    public EndGameManager endGameManager;

    private void Start()
    {
        healthBar.SetMaxHealth(maxHealth);
    }
    public void OnGUI()
    {
        if (photonView.IsMine)
        {
            health.text = "Player Health : " + currentHealth;
            
            
            healthBar.SetHealth(currentHealth);
        }
        else
            healthBar.gameObject.SetActive(false);



    }

    public void TakeDamage(int amount, int OwnerNetID)
    {
        if (photonView != null)
            photonView.RPC("PunRPCTakedDamage", RpcTarget.All, amount, OwnerNetID);
        else print("photonView is NULL.");
    }

    [PunRPC]
    public void PunRPCTakedDamage(int amount, int OwnerNetID)
    {
        Debug.Log("Take Damage");
        currentHealth -= amount;

        

        if (currentHealth <= 0)
        {
            Debug.Log("Dead");
            //currentHealth = maxHealth;
            endGameManager.EndGame();
        }
    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(currentHealth);
        }
        else
        {
            currentHealth = (int)stream.ReceiveNext();
        }
    } 
    public void Healing(int amout)
    {
        currentHealth += amout;
    }
    

}